#ifndef OS_COMPAT_H
#define OS_COMPAT_H

#define _GNU_SOURCE
#define _LARGE_FILES
#define _FILE_OFFSET_BITS 64
#define _BSD_SOURCE /* for mincore */
#define _XOPEN_SOURCE 600 /* for posix_fadvise */
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <assert.h>
#include <stdint.h>

#ifndef O_NOATIME
#  define O_NOATIME 0
#endif /* O_NOATIME */

#define PAGE_MASK               (~(page_size() -1))
#define PAGE_ALIGN(addr)        (((addr) + page_size() - 1) & PAGE_MASK)
#define PAGE_ALIGN_DOWN(addr) \
	(addr > page_size() ? PAGE_ALIGN(addr) - page_size() : 0)

static inline size_t page_size(void)
{
	static size_t size;

	if (!size)
		size = sysconf(_SC_PAGESIZE);

	return size;
}

#define ARRAY_SIZE(x) (sizeof(x)/sizeof(x[0]))

/*
 * converts a C string to a non-negative off_t value, taking base into
 * account.  On error, it'll return a negative value and set errno
 * to EINVAL
 */
static inline off_t cstr_to_off_t(const char *nptr, char **endptr, int base)
{
	if (sizeof(long) == 8 || (sizeof(long) == 4 && sizeof(off_t) == 4))
		return (off_t)strtol(nptr, endptr, base);
	else if (sizeof(off_t) == 8 && sizeof(long) == 4)
		return (off_t)strtoll(nptr, endptr, base);

	fprintf(stderr, "unrecognized sizes:\n\toff_t: %u\n\tlong: %u\n",
	                (unsigned)sizeof(off_t), (unsigned)sizeof(long));
	exit(1);
}

static inline int open_noatime(const char *path)
{
	int flags = O_RDONLY | O_NOATIME;
	int fd = open(path, flags);

	if (fd < 0) {
		if (errno == EPERM && O_NOATIME != 0)
			fd = open(path, O_RDONLY);
		return fd;
	}

	return fd;
}

#endif /* OS_COMPAT_H */
