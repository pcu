#include "compat-util.h"
#include <dirent.h>
#include <libgen.h>
#include <dlfcn.h>

#if defined(_POSIX_SYNCHRONIZED_IO) && _POSIX_SYNCHRONIZED_IO > 0
static int have_fdatasync = 1;
#else
static int have_fdatasync;
#endif

/* TODO: sync_file_range() if on Linux */

static int usage(const char * argv0)
{
	fprintf(stderr, "Usage: %s [-d] [-D] FILE...\n", argv0);
	return 1;
}

#define FN_NOT_FOUND ((void *)(1))

static int fs_sync(const char *path)
{
	int rc = 0;
#if defined(__linux__) && defined(RTLD_NEXT)
	static int (*syncfs_fn)(int);

	if (syncfs_fn == NULL) {
		syncfs_fn = dlsym(RTLD_DEFAULT, "syncfs");
		if (syncfs_fn == NULL || dlerror())
			syncfs_fn = FN_NOT_FOUND;
	}
	if (syncfs_fn != NULL && syncfs_fn != FN_NOT_FOUND) {
		int fd = open(path, O_RDONLY|O_NOATIME);

		if (fd >= 0) {
			int err;
			rc = syncfs_fn(fd);
			err = errno;
			close(fd);

			/*
			 * if glibc has syncfs(2) but we're running an
			 * old kernel, fall back to sync(2) below
			 */
			if (err != ENOSYS)
				return rc;
			rc = 0;
		}
	}
#endif /* ! __linux__ */
	sync();
	return rc;
}

static int do_sync(const char *path, int data_only, int directory)
{
	int fd;
	const char *errfunc = "";

	if ((fd = open(path, O_RDWR|O_NOATIME)) < 0) {
		if (errno == EISDIR) {
			directory = 1;
			goto sync_dir;
		}
		errfunc = "open";
		goto err;
	}

	if (data_only && have_fdatasync) {
		if (fdatasync(fd) < 0) {
			errfunc = "fdatasync";
			goto err;
		}
	} else {
		if (fsync(fd) < 0) {
			errfunc = "fsync";
			goto err;
		}
	}

	if (close(fd) < 0) {
		errfunc = "close";
		goto err;
	}

sync_dir:
	if (directory) {
		char *dpath;
		DIR *dir;

		if (!(dpath = strdup(path))){
			errfunc = "strdup";
			goto err;
		}
		if (!(dir = opendir(dirname(dpath)))) {
			errfunc = "opendir";
			goto err;
		}
		if (fsync(dirfd(dir)) < 0) {
			errfunc = "(directory) fsync";
			goto err;
		}
		if (closedir(dir) < 0) {
			errfunc = "closedir";
			goto err;
		}
		free(dpath);
	}

	return 0;
err:
	fprintf(stderr, "%s: %s(): %s\n", path, errfunc, strerror(errno));
	return -1;
}

int main(int argc, char * const argv[])
{
	int data_only = 0;
	int fs = 0;
	int directory = 0;
	int opt;
	int argi = 1;

	while ((opt = getopt(argc, argv, "dDf")) != -1) {
		++argi;
		switch(opt) {
		case 'd':
			data_only = 1;
			break;
		case 'D':
			directory = 1;
			break;
		case 'f':
			fs = 1;
			break;
		default:
			return usage(argv[0]);
		}
	}

	if (argi >= argc)
		return usage(argv[0]);

	for (; argi < argc; ++argi) {
		if (fs) {
			if (fs_sync(argv[argi]) < 0)
				return 1;
		} else {
			if (do_sync(argv[argi], data_only, directory) < 0)
				return 1;
		}
	}

	return 0;
}
