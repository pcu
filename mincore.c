#include "compat-util.h"
static int summary;

static int usage(const char * argv0)
{
	fprintf(stderr,
	        "Usage: %s [-o OFFSET] [-l LENGTH] [-s] FILE...\n", argv0);
	return 1;
}

static void mincore_stats(const char *path, off_t offset, off_t len)
{
	char *map;
	unsigned char *vec;
	size_t vec_len;
	size_t map_len;
	off_t map_offset;
	int fd;
	size_t i;
	static const char *fmt = sizeof(void *) == 8 ?
	                         "%s: %016lx %x\n": "%s: %08lx %x\n";

	if ((fd = open_noatime(path)) < 0) {
		fprintf(stderr, "%s: open(): %s\n", path, strerror(errno));
		return;
	}

	if (!len) {
		struct stat sb;

		if (fstat(fd, &sb) < 0) {
			fprintf(stderr, "%s: fstat(%d): %s\n",
				path, fd, strerror(errno));
			goto err_close;
		}
		len = sb.st_size - offset;
	}

	vec_len = (len + page_size() - 1) / page_size();
	if (!(vec = malloc(vec_len))) {
		fprintf(stderr, "%s: malloc(%lu): %s\n",
		        path, (unsigned long)vec_len, strerror(errno));
		goto err_close;
	}

	map_len = PAGE_ALIGN(len);
	map_offset = PAGE_ALIGN_DOWN(offset + 1);

	map = mmap(NULL, map_len, PROT_READ, MAP_SHARED, fd, map_offset);
	if (map == MAP_FAILED) {
		fprintf(stderr, "%s: mmap(%lu): %s\n",
		        path, (unsigned long)vec_len, strerror(errno));
		goto err_free;
	}

	if (mincore(map, map_len, vec) < 0) {
		fprintf(stderr, "%s: mincore(%lu): %s\n",
		        path, (unsigned long)vec_len, strerror(errno));
		goto err_munmap;
	}

	if (summary) {
		size_t n = 0;

		for (i = 0; i < vec_len; ++i)
			if (vec[i] & 1)
				++n;
		printf("%s: %F\n", path, (double)n / (double)vec_len);
	} else {
		for (i = 0; i < vec_len; ++i)
			printf(fmt, path,
			       (unsigned long)((page_size() * i) + map_offset),
			       vec[i] & 1);
	}
err_munmap:
	munmap(map, map_len);
err_free:
	free(vec);
err_close:
	close(fd);
}

int main(int argc, char * const argv[])
{
	off_t offset = 0;
	off_t len = 0;
	int argi = 1;
	int opt;

	while ((opt = getopt(argc, argv, "o:l:hs")) != -1) {
		char *err;

		++argi;
		switch(opt) {
		case 'o':
			++argi;
			offset = cstr_to_off_t(optarg, &err, 10);
			if (*err || offset < 0) {
				fprintf(stderr, "offset must be >= 0\n");
				return 1;
			}
			break;
		case 'l':
			++argi;
			len = cstr_to_off_t(optarg, &err, 10);
			if (*err || len < 0) {
				fprintf(stderr, "length must be >= 0\n");
				return 1;
			}
			break;
		case 's':
			summary = 1;
			break;
		default:
			return usage(argv[0]);
		}
	}

	if (argi >= argc)
		return usage(argv[0]);

	for (; argi < argc; ++argi)
		mincore_stats(argv[argi], offset, len);
	return 0;
}
