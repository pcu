# default target
all::

prefix = $(HOME)
bindir = $(prefix)/bin
mandir = $(prefix)/share/man
INSTALL = install
PANDOC = pandoc
PANDOC_OPTS = -s -f markdown --email-obfuscation=none
pandoc = $(PANDOC) $(PANDOC_OPTS)
pandoc_html = $(pandoc) --toc -t html --no-wrap
STRIP ?= strip
GIT-VERSION-FILE: .FORCE-GIT-VERSION-FILE
	@./GIT-VERSION-GEN
-include GIT-VERSION-FILE

DISTNAME = pcu-$(GIT_VERSION)

# overridable from command-line
CFLAGS = -g -O2 -Wall
LDFLAGS = -Wl,-O1

pcu-mincore: mincore.c compat-util.h
pcu-fadvise: fadvise.c compat-util.h
pcu-fsync: fsync.c compat-util.h

PCU_BIN := pcu-fadvise pcu-mincore pcu-fsync

pcu-fsync: LDFLAGS += -ldl

$(PCU_BIN):
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@+ $<
	mv $@+ $@

all:: $(PCU_BIN)

install: $(PCU_BIN)
	$(INSTALL) -d -m 755 '$(DESTDIR)$(bindir)'
	$(INSTALL) $^ '$(DESTDIR)$(bindir)'

strip: $(PCU_BIN)
	$(STRIP) $(STRIP_OPTS) $(PCU_BIN)

install-strip: strip
	$(MAKE) install

%.1: %.1.txt
	$(pandoc) -t man < $< > $@+
	mv $@+ $@
%.1.html: %.1.txt
	$(pandoc_html) -T $(basename $@) < $< > $@+
	mv $@+ $@

man1 := $(addsuffix .1,$(PCU_BIN))
html := index.html INSTALL.html $(addsuffix .html, $(man1))
html: $(html)
man: $(man1)
doc: html man
install-man: install-man1
install-man1: $(man1)
	$(INSTALL) -d -m 755 '$(DESTDIR)$(mandir)/man1'
	$(INSTALL) $^ '$(DESTDIR)$(mandir)/man1'

dist: GIT-VERSION-FILE
	git archive --format=tar --prefix=$(DISTNAME)/ HEAD^{tree} \
		| gzip -9 > $(DISTNAME).tar.gz+
	mv $(DISTNAME).tar.gz+ $(DISTNAME).tar.gz
clean:
	$(RM) $(PCU_BIN) $(DISTNAME).tar.gz* GIT-VERSION-FILE
	$(RM) $(man1) $(html)

INSTALL.html index.html: title = $(shell sed -ne 1p < $<)
INSTALL.html: INSTALL
index.html: README

index.html INSTALL.html:
	$(pandoc_html) -T "$(title)" < $< > $@+
	mv $@+ $@

txt := $(addsuffix .1.txt,$(PCU_BIN))
www: index.html INSTALL.html INSTALL README $(man1) $(html) $(txt)
	mkdir -p public_html/
	install -m 644 $^ public_html/
	cd public_html && for i in $^; do \
	  gzip -9 < $$i > $$i.gz; \
	  touch -r $$i.gz $$i; \
	  done

.PHONY: .FORCE-GIT-VERSION-FILE install install-man man
